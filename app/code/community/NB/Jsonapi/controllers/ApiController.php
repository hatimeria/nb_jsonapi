<?php
class NB_Jsonapi_ApiController extends Mage_Api_Controller_Action
{

	public function parsePayload(){
    	$payload = json_decode($this->getRequest()->getRawBody(),true);
		foreach ($payload as $key => $value) {
			$this->getRequest()->setParam($key,$value);
		}
	}

    public function callAction()
    {
    	
    	$this->parsePayload();
        
        $this->_getServer()->init($this, 'json')->run();

	}
} // Class NB_Jsonapi_ApiController End
