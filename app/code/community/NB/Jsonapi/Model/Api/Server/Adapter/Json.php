<?php
class NB_Jsonapi_Model_Api_Server_Adapter_Json
    extends Varien_Object
    implements Mage_Api_Model_Server_Adapter_Interface
{

     /**
     * Set handler class name for webservice
     *
     * @param string $handler
     * @return NB_Vudu_Model_Api_Server_Adapter_Rest
     */
    public function setHandler($handler)
    {
        $this->setData('handler', $handler);
        return $this;
    }

    /**
     * Retrive handler class name for webservice
     *
     * @return string
     */
    public function getHandler()
    {
        return $this->getData('handler');
    }

     /**
     * Set webservice api controller
     *
     * @param Mage_Api_Controller_Action $controller
     * @return NB_Jsonapi_Model_Api_Server_Adapter_Json
     */
    public function setController(Mage_Api_Controller_Action $controller)
    {
         $this->setData('controller', $controller);
         return $this;
    }

    /**
     * Retrive webservice api controller
     *
     * @return Mage_Api_Controller_Action
     */
    public function getController()
    {
        return $this->getData('controller');
    }

    /**
     * Run webservice
     *
     * @param Mage_Api_Controller_Action $controller
     * @return NB_Jsonapi_Model_Api_Server_Adapter_Json
     */
    public function run()
    {

        $request = $this->getController()->getRequest();

        $return = array();

        $user = $request->getParam('user');
        $token = $request->getParam('token');

        $resource = $request->getParam('resource_path');
        $args = $request->getParam('args');

        $handler = $this->getHandler();

        if(is_string($handler)){
            $handler = Mage::getSingleton($handler);
        }

        try{
            $session = $handler->login($user,$token);
            $return['results'] = $handler->call($session,$resource,$args);
        } catch(Exception $e){
            $return['fault'] = $e->getMessage();
        }

        try{
            $jsonBody = json_encode($return);
        } catch(Exception $e){
            $return['fault'] = $e->getMessage();
        }

        $this->getController()->getResponse()
            ->clearHeaders()
            ->setHeader('Content-Type','application/json')
            ->setBody($jsonBody);

        return $this;
    }

    /**
     * Dispatch webservice fault
     *
     * @param int $code
     * @param string $message
     */
    public function fault($code, $message)
    {
        throw new Exception($message, $code);
    }
} // Class NB_Jsonapi_Model_Api_Server_Adapter_Json End
